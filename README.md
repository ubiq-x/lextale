# Lexical Test for Advanced Learners of English (LexTALE)
An experiment for administering the Lexical Test for Advanced Learners of English (LexTALE) task for monolinguals and bilinguals of English and Spanish to be used with SR Research [Experiment Builder](https://www.sr-research.com/experiment-builder).


## Abstract
Research on bilingualism often demands an accurate measurement of language proficiency in either or both the primary and secondary language.  One task which provides such a measurement is the Lexical Test for Advanced Learners of English (LexTALE) task (Lemhöfer & Broersma, 2012).  LexTALE estimates linguistic proficiency by measuring vocabulary knowledge.  This repository presents an implementation of this task which can be run in the Experiment Builder software.  More specifically, the task implemented accommodates both monolinguals and bilinguals of English and Spanish and can easily be extended to other languages.  Additionally, an R script for the analysis of data collected with this task is presented as well.


## Contents
- [Features](#features)
- [Details](#details)
  * [The Task](#the-task)
  * [Subject Types](#subject-types)
  * [Instructions](#instructions)
  * [Items](#items)
  * [LexTALE for English](#lextale-for-english)
  * [LexTALE for Spanish](#lextale-for-spanish)
  * [Results](#results)
- [Data Analysis](#data-analysis)
  * [Data Preparation](#data-preparation)
  * [Graphical Output](#graphical-output)
  * [Textual Output: By Language Group](#textual-output-by-language-group)
  * [Textual Output: By Language Group and Subject](#textual-output-by-language-group-and-subject)
- [Dependencies](#dependencies)
- [Setup](#setup)
- [Acknowledgments](#acknowledgments)
- [References](#references)
- [Citing](#citing)
- [License](#license)


# Features
- Administer the LexTALE task for monolinguals and bilinguals of English and Spanish
- Automatically compute both English and Spanish scores
- Requires SR Research [Experiment Builder](https://www.sr-research.com/experiment-builder)


## Details
### The Task
Quoting from the [LexTALE Web site](http://www.lextale.com/whatislextale.html) (visit for more details):

> The LexTALE is a quick and practically feasible test of vocabulary knowledge for medium to highly proficient speakers of English as a second language. It consists of a simple un-speeded visual lexical decision task. In contrast to other vocabulary or proficiency tests, it has been designed to meet the needs of cognitive researchers. It is quick, easy to administer, and free, and yet it is a valid and standardized test of vocabulary knowledge. It has also been shown to give a fair indication of general English proficiency.

Despite the fact that the above description mentions only English, the task is also available for other languages like Dutch, German, or Spanish (although whether its name should or should not still contain the letter E at the end certainly is an interesting philosophical question).

The task presented in this repository is a self-contained LexTALE task that can be used to estimate proficiency of both monolinguals and bilinguals of English and Spanish.

### Subject Types
Before the task commences, the experimenter selects the type of the subject:

- Monolingual English
- Monolingual Spanish
- Bilingual

This selection is significant because monolinguals only go through the task corresponding to their language while bilinguals go through both English and Spanish tasks.  Moreover, Monolinguals see instructions in their language while bilinguals see them in both languages.

### Instructions
Subject instructions are saved as sets of images in the [`eb/library/images`](eb/library/images) directory.  There are two sets that correspond to the following screen resolutions: `1680x1050` and `1600x1200`.  The experiment automatically chooses the set matching the current screen resolution as defined in the Experiment Builder project properties.

If different resolution is required, new images need to be generated (e.g., using the [Medusa](https://gitlab.com/ubiq-x/medusa) software) or the existing ones need to be rescaled; the new images need to be named properly because the experiment chooses them based on their name.

### Items
LexTALE presents subjects with a sequence of string characters being either a word or non-word, item by item, and the subject is asked to indicate whether the string is a word or not.  The lists of those character strings are embedded into the following data sets which are loaded into the appropriate structures by the Experiment Builder and therefore do not need to be changed:

- English: [`eb/datasets/SEQ_EN_DataSource_Experiment_5dSEQ_EN.dat`](eb/datasets/SEQ_EN_DataSource_Experiment_5dSEQ_EN.dat)
- Spanish: [`eb/datasets/SEQ_SP_DataSource_Experiment_5dSEQ_SP.dat`](eb/datasets/SEQ_SP_DataSource_Experiment_5dSEQ_SP.dat)

For example the items with IDs of `4` and `5` from the English data set are:
```
4  "ablaze"    1
5  "kermshaw"  0
```
The third column indicates whether the corresponding item is a word (`0` denotes a non-word).

### LexTALE for English
The English test consists of 60 items and the uses the following score formula (based on Lemhöfer & Broersma, 2012 and the [LexTALE Web site](http://www.lextale.com/scoring.html)):
```
score.en = ((n.correct.words / 40 * 100) + (n.correct.non.words / 20 * 100)) / 2
```

### LexTALE for Spanish
The English test consists of 90 items and uses the following score formula (based on Izura, et al., 2014):
```
score.sp = n.correct.words – 2 * n.correct.non.words
```

### Results
The present experiment stores all result for every subject in a set of tab-delimited text files.  The main file, named `lextale-en.txt` for the English task, contains details on every item presented to the subject.  Below is an example of how the first nine lines from that file might look like (tabs replaced with spaces and comments added for clarity):
```
id  str       is.word  resp        rt
 0  platery         0     0  3255.819  # id=0 indicates items not used in score computation
 0  denial          1     1  1185.915  # reaction time of 1185.915 ms
 0  generic         1     1   896.385
 1  mensible        0     1  2378.435  # non-word responded to as 'word'
 2  scornful        1     1   784.569  # word responded to as 'word'
 3  stoutly         1     1  1341.658
 4  ablaze          1     1  1212.049
 5  kermshaw        0     0  1162.555  # non-word responded to as 'non-word'
```

The experiment also creates a short summary file, named `lextale-en-summary.txt` for the English task, which might look like this:
```
Words presented: 40
Non-words presented: 20

Words correct: 40
Words incorrect: 0
Non-words correct: 17
Non-words incorrect: 3

Score (Lemhofer and Broersma, 2012; average % correct): 0.925
```

There are two corresponding files created for the Spanish language as well.


## Data Analysis
In this section, I describe one way in which the results collected with the present task can be analyzed.

### Data Preparation
Because Experiment Builder stores each subject's results in separate directories, the first step is to copy all the LexTALE result file to one directory but preserving the subject IDs as the names of subdirectories because they become useful later.  The [`analysis/data/lextale/`](analysis/data/lextale/) directory contains an example of how that data might end up being structured (this is part of real data collected with the present task).

### Graphical Output
Once the data directory has been prepared, the [`analysis/analysis.R`](analysis/analysis.R) R script can be used to produce a plot like the one shown in Figure 1 (I leave fixing the diamond symbol x-coordinate as an exercise to the reader).

<p align="center"><img src="analysis/out/img/lextale.png" width="50%" /></p>
<p align="center"><b>Figure 1.</b> LexTALE scores per language group.</p>

### Textual Output: By Language Group
Additionally to the above image, the R script also produces useful textual output.  The first type of textual output is stored in [`analysis/out/txt/insight.lextale-01.txt`](analysis/out/txt/insight.lextale-01.txt) and contains basic statistics of by-language-group separation, e.g.:
```
   grp en_score_p_m en_score_p_sd sp_score_p_m sp_score_p_sd
1 b.en           84             9           73            18
2 b.sp           90             8           84            10
3 m.en           92             6           49             5
4 m.sp           73             8           95             4
```
The specific four language groups are:

- `b.en` - bilingual English
- `b.sp` - bilingual Spanish
- `m.en` - monolingual English
- `m.sp` - monolingual Spanish

This particular group composition reflects the aims of the original research which are largely irrelevant here.  What is relevant, however, is that the idea behind two bilingual groups (instead of just one) is that they cumulate subjects with the primary language being either English or Spanish.  A different type of research might opt to group the two types of bilingual subjects together.

### Textual Output: By Language Group and Subject
The second type of textual output is stored in [`analysis/out/txt/insight.lextale-02.txt`](analysis/out/txt/insight.lextale-02.txt) and aggregates the data on a lower by-language-group-and-subject level, e.g.:
```
    grp sub en_nwc en_nwi en_nnc en_nni en_n sp_nwc sp_nwi sp_nnc sp_nni sp_n en_score_p sp_score sp_score_p
1  b.en 001     40      0     18      2   60     48     12     16     14   90      95.00       20      66.67
2  b.en 004     33      7     18      2   60     47     13     28      2   90      86.25       43      85.83
3  b.en 007     38      2     11      9   60     45     15     15     15   90      75.00       15      62.50
4  b.en 019     35      5     13      7   60     34     26     15     15   90      76.25        4      53.33
5  b.en 049     39      1     16      4   60     60      0     28      2   90      88.75       56      96.67
6  b.sp 005     34      6     19      1   60     59      1     29      1   90      90.00       57      97.50
7  b.sp 008     28     12     18      2   60     42     18     29      1   90      80.00       40      83.33
8  b.sp 009     40      0     20      0   60     45     15     22      8   90     100.00       29      74.17
9  b.sp 018     39      1     17      3   60     49     11     24      6   90      91.25       37      80.83
10 m.en 014     40      0     17      3   60      7     53     24      6   90      92.50       -5      45.83
11 m.en 017     29     11     19      1   60     20     40     23      7   90      83.75        6      55.00
12 m.en 022     40      0     19      1   60     38     22      8     22   90      97.50       -6      45.00
13 m.en 030     36      4     20      0   60     32     28     13     17   90      95.00       -2      48.33
14 m.sp 002     21     19     17      3   60     57      3     27      3   90      68.75       51      92.50
15 m.sp 003     30     10     18      2   60     60      0     30      0   90      82.50       60     100.00
16 m.sp 047     28     12     13      7   60     57      3     27      3   90      67.50       51      92.50
```
This output is more verbose than the previous one in that it contains information about correct and incorrect responses to the LexTALE task items.


## Dependencies
- The present experiment needs to be run by the [Experiment Builder](https://www.sr-research.com/experiment-builder)


## Setup
No setup is required; cloning this repository is enough.


## Acknowledgments
I worked as a post-doctoral research associate at the University of Pittsburgh when I developed this experiment.


## References
Izura C., Cuetos F., & Brysbaert M. (2014).  Lextale-Esp: A Test to Rapidly and Efficiently Assess the Spanish Vocabulary Size.  _Psicológica, 35_, 49-66.

Lemhöfer K. & Broersma M. (2012).  Introducing LexTALE: A Quick and Valid Lexical Test for Advanced Learners of English.  _Behavioral Research Methods, 44(2)_, 325–343.

[www.lextale.com](http://www.lextale.com)


## Citing
Loboda, T.D. (2016).  LexTALE [Experiment Builder experiment].  Available at https://gitlab.com/ubiq-x/lextale


## License
This project is licensed under the [BSD License](LICENSE.md).
